聖女瑪蒂婭的聲音回蕩於貝爾菲因的街道。其臉頰上留下了血沫的痕跡。小小的嘴唇不住地顫抖。
“止血藥，止血用的藥草也沒關係。將所有能成為藥物的東西全部收集起來。”
瑪蒂婭一邊放出顫抖的聲音，一邊注視著眼前的重傷者。剛纏好的綳帶上，已經滲出了血。

不行。果然，負的傷還是太重了。撕裂肩膀的那個裂傷，雖然談不上致命傷,不過，就這樣下去的話會因為失血過多而死的。

瑪蒂婭揚起眉，睜大眼睛，注視著眼前的重傷者——鋼鐵公主薇斯塔莉努的臉。意識還沒有恢復。不知道是不是失血過多的緣故，臉上帶著一絲寒意。

很明顯，狀態很差。如果不強行止住血，她的生命之燈就會慢慢熄滅。瑪蒂婭一邊咬著嘴唇，一邊在心中不由自主地咂著舌頭。
路易斯，我有能做到的事，也有做不到的事情。

真是的，不愧是他。在拜托人救助傷員之後送來了這樣的重傷者，真是太惡劣了。既然救了誰，那麼就應該竭盡全力去救活那條生命的吧？
瑪蒂婭自然而然地想起了在小巷中和路易斯分別前說的話。
——也許我自身會被卷進去，但我想拜託你作為最後的堡壘。
他輕飄飄地這樣說完後就消失在了戰場上。
然後，被送進來的，就是受了重傷的她，鋼鐵公主薇斯塔莉努，還有一名自稱是布魯達的同樣渾身鮮血的傭兵。

但是，能做到的事情極其有限。不知道是誰讓這位鋼鐵公主受了這樣的重傷，如果這傷再稍微嚴重點的話…雖然深處沒裂開還算好，不過…
“怎麼樣了，小姐？薇斯她…不，薇斯塔莉努。”
不行了嗎，不斷顫抖著的聲音，從布魯達的嘴唇中吐出。那是已經死心了的聲音。這個裂傷，噴出的血液，奪走了布魯達的希望。

瑪蒂亞不知道布魯達和薇斯塔莉努之間的關係。倒不如說，從聽說的內容看來，她們是敵對的才對。但是聽到剛才的聲音，以及拚命把薇斯塔莉努送進來的那副樣子，瑪蒂婭就很清楚了，薇斯塔莉努對布魯達來說是無法忍受失去的存在。瑪蒂亞的心臟發出奇怪的聲音。

那副身姿，與腦海中常浮現的一個人重疊了。心中浮現出了一種共鳴般的感情。如果是以前，應該會笑著說真是愚蠢，那樣的感情。
深呼吸一次，瑪蒂婭眯起了眼，紋章教徒拿來了少量的藥。
“——把火拿來。燒焦傷口，止住血液的流動。”
一邊從懷裡取出一把短刀，一邊慢慢地這樣說。
無論如何都必須幫忙。路易斯去幫助的是傭兵布魯達，而布魯達把薇斯塔莉努帶來了，那麼她也一定是應該幫助的存在。

而且，路易斯告訴自己，想讓你成為最後的堡壘。正因為如此，回應那希望，就是我的責任吧？還有…
瑪蒂婭感覺到，曾經親自用手勒住，應該停止了呼吸的情感，一點點回到了胸中。心跳不由得加速。
——而且，我是聖女。在眼前拚命伸出手來求救的人是不可能無視掉的。
我知道。我明白。迄今為止，我在戰場上對多少人見死不救？為了自己的計劃坑害了多少人？事到如今，還想裝成聖女來拯救他人的生命，真是讓人笑不出來啊。一定是自己的神，在嘲弄著現在的我。

這是偽善，這是欺騙。不過是一切愚蠢的行為中的一種。啊啊，那明明應該是自己最忌諱的行為。
但是，我不想再做那種放棄的事了，因為沒有辦法而放棄的事。

這也是，全部都是因為他。因為看到他為了絕對是不合理，甚至讓人覺得沒有意義的事而拚命伸出手的樣子。真的是讓人無可奈何的傢伙，必須要一直守護著他的身影的那個傢伙。

用火焰把鐵燒熱，將發熱的短劍按在薇斯塔莉努肩上。微弱的嗚咽，從薇斯塔莉努的嘴唇中流露出來。她朦朧的眼睛，微微睜開。
“誰給點水給她？”
只要能稍稍恢復意識，就可以喝水。只要能喝水的話，多少能延續生命吧。從那之後，就要看她自己的命運了。

一邊簡短地說著，瑪蒂婭又將鐵劍貼在薇斯塔莉努的肩膀上。肉被燒焦的臭味刺激著鼻孔，手上可以直接感受到肉體顫抖的感覺。自己的呼吸也因緊迫感而變得狂亂，無法保持正常。好不容易才意識到周圍已經被夜色沾染了。
——然後，我終於注意到了，扭曲的光線籠罩著周圍。
不由得眨了眨眼。視線稍稍從薇斯塔莉努身上離開。覆蓋著貝爾菲因的淡綠色光芒，散發著令人毛骨悚然的光芒。強大的魔力之光，瑪蒂婭睜大了眼睛，嘴唇顫抖著。

現在是應該停留在這裡，還是應該趁早離開？
太過異常的事態，使得大腦搖擺不定。理性大聲呼籲，留在這裡真的是正確的嗎？耳中傳來了薇斯塔莉努的呻吟聲。瑪蒂婭眨了眨眼。

不行。果然現在的自己是不行的。作為紋章教的領袖，明明應該把紋章教的事放在第一位，明明應該是紋章教的聖女。現在卻無論如何都不想離開這裡。

那是因為，眼前有必須救助的人。因為這裡是他最後的堡壘。

確認血止住了之後，將短劍從薇斯塔莉努的傷口上移開。然後在去摸索藥草的時候，突然，注意到了。環視四周，可是，哪裡都沒有。剛才還在往薇斯塔莉努的嘴裡灌水，一直盯著那張臉的人的身影。

布魯達不見了。
瑪蒂婭的後背好像有什麼直覺似的戰慄著，嘴唇不由得扭曲了。
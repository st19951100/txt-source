――――――然後。



戴姆庫爾德的一切都被青炎包圍。







殘留在死亡世界的最後人類。

被約定的方舟拾起的所有生命，都消失在藍色的火焰中。



魔王無言地睥睨著應殘留下來的東西無慈悲地燃燒。

沒有驚訝。

只是這麼想著。



―――啊啊，果然。



瞳孔中映照著籠罩了大半惑島和衛星島的青炎，魔王向背後打招呼。

在那裡―――

有他親手殺害的同學。



「信任了一個笨蛋啊」



不可能會有回答。

他們已經不動了。

因為魔王親自斷絕了他們的生命。



可是，青炎證明了。

她的存在。

她的存在。











「―――啊嘞~？　暴露了嗎？」











mukuli、と。

理所當然般的，亞澤麗雅站了起來。



紅頭髮的少女用自己的腳站起來，看到沾滿鮮血的長袍皺起眉頭。

她死心一樣地歎了口氣，啪啪地用手拍去長袍上的污垢，露出笑容。



「好久不見了，哥哥♪　你怎麼知道是我？」



亞澤麗雅――

不。

自稱妹妹的惡魔。

簡直就像在看猜謎節目一樣，輕鬆地問著前世的哥哥。



「……你總是轉生成和我親近的女性」



魔王回頭了。

黃昏色的劍，即使在黑夜中也沒有失去光輝。



「阿涅莉也是，菲爾也是。所以我覺得如果建了後宮，你就會混進去……但看來不是這樣。

這樣的話，亞澤麗雅就是第一後補。這只是單純的排除法」



引導世界走上終焉的黃昏色的劍，在真正的敵人面前更加閃耀。

魔王將劍尖。

指向有著亞澤麗雅姿態的怪物。



「我（一直）等著這個時刻。

你這傢伙已經沒有能轉生逃走的地方了―――！！」



必定殺死。

確實的殺死。

把全體人類作為祭品，終於把她逼到絕境……！！



「呋……呋呋呋！」



本應被逼到絕境的妹妹―――

卻露出了妖艷的笑容。



「啊啊……啊啊、啊啊、啊啊！

好開心！　我真的很開心，哥哥！

哥·哥·是·為·了·我·掃·除·了·多·餘·的·人·類·吧·？

好感動！　本來想自己做的，但哥哥竟然比我先動手了！

這樣就能充分、不用顧忌地培育愛了呢，哥哥！」



魔王的表情像看到恐怖的東西一樣扭曲了。

語言相通卻不通。

不管說什麼，不管做什麼，這個惡魔都會按自己方便來解釋。



這種東西不能存在於世。

就算是已經結束的世界，也沒有這傢伙存在的地方。



問答無用。

接收了魔王的殺意，黃昏色的光輝撕裂黑暗。



這以上。

就算是一瞬間。

為了不容許那個存在。



魔王、對曾經是妹妹的東西邁出一步。







―――之後。



停了下來。







地上的人類死絕了。

逃到天空的人也燃盡了。

世界上只剩下在這裡的兩個人。

本應如此。



可是――

現在。

魔王聽到的是。



腳·步·聲·。



不屬於這裡兩人任何一方的腳步聲――

――以拖著腳般的狀態，慢慢向這邊靠近。



「……？」



有著亞澤麗雅身姿的妹妹，也訝異地看向聲音傳來的方向。



堆積的瓦礫。

在瓦礫山之間。

一個人出現了。



「……啊……」



魔王這次終於露出了驚愕的表情。

那是本應不在了的人類。

是他親自捨棄了的人類。



青色的頭髮。

尖尖的耳朵。

衣服保持著被他撕裂的狀態。



拖著一隻腳，爬著來到這裡的是―――







「…………瑞、秋…………？」







屍體。

沒錯。

確實沒有確認。



有致命傷的反應。

可是。

可是啊。

她的精霊術【神意的接收】。

通過模仿他人的精霊術而能行使同樣的術，在她能用的精靈術中，有治療傷口的【治癒的先導】。



惑島底下。

如果在任何人都不會發現的懸崖下，專心應急處理的話……。

說不定能保住一命。

就像現在這樣，沒有被任何人注意到。



消耗了許多體力的瑞秋，看見了有著亞澤麗雅身姿的『那傢伙』。



「…………誒…………？」



然後。

這樣嘟囔到。







「…………你、是誰…………？」







『那傢伙』從外表上怎麼看都是亞澤麗雅。

你是誰――と。



瞬間，魔王心中本應消失了的感情狂暴著。



明白嗎？

你明白嗎？



我認為誰都不會理解。

我認為誰都不能理解。




但是。

你。

你能明白嗎！

瑞秋―――！！



「……啊啊」



『那傢伙』。

用和亞澤麗雅相同卻不同的臉和聲音，無聊地說到。



「羽蟲、還剩下了一隻嗎」



反射性的。



「住――――！！」



願望沒能實現。

祈禱來得太晚了。



『那傢伙』手中發出的青炎包裹了瑞秋全身。







◆◆◆―――――――◆◆◆―――――――◆◆◆







感覺不到熱。

在來到這裡的途中，已經習慣了疼痛和痛苦，現在就算被燃燒全身，也不覺得可怕。



瑞秋無力地倒下，看著青炎對面的傑克的臉。



（……啊啊）



最後點亮的是、

真的是一個小小的、

願望。



（……不要、露出那種表情啊、傑克……）



傑克把混雜著憎惡、怨恨和憤怒的淒絕表情，轉向了有著亞澤麗雅姿態的某人。



黃昏色的劍被揮舞著。

看上去像亞澤麗雅的某人，隨心所欲開心地讓青炎四處奔走。



真真正正的世界最後的戰鬥開始了。

可是―――

瑞秋在看到開幕之前。

就失去了——



意識和。

生命。


fin
